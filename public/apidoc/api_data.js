define({ "api": [
  {
    "type": "get",
    "url": "api/dictionary/list",
    "title": "字典列表",
    "description": "<p>字典列表</p>",
    "name": "dictionary-list",
    "group": "dictionary",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageNum",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "pageSize",
            "description": "<p>每页多少条</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "combox_name",
            "description": "<p>字典名称</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "result",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"success\" : \"true\",\n    \"result\" : {\n        \"pageNum\" : \"1\",\n        \"pageSize\" : \"10\",\n        \"combox_name\": \"combox_name\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://localhost:5095/api/dictionary/list"
      }
    ],
    "version": "1.0.0",
    "filename": "routes/api_t_dictionary.js",
    "groupTitle": "dictionary"
  }
] });
