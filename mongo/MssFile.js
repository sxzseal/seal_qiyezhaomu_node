var mongoose = require('../util/mongo'),
Schema = mongoose.Schema;

var FileSchema = new Schema({
    filename: { type: String },                    //文件名
    uploadDate: { type: Date },                        //上传时间
    length: { type: Number },                        //大小
});


module.exports = mongoose.model('fs.files', FileSchema);