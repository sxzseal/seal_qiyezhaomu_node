/********************************
 * create by seal 20230220
 * @type {*|createApplication}
 * 模块相关api
 ********************************/
var express = require("express");
var router = express.Router();
var db = require("../../util/mysql_pool");
var apiUtil = require("../../util/apiUtil");
var util = require("../../util/util");
var logger = require("log4js").getLogger("request");
var jwt = require("jwt-simple");
var moment = require("moment");
var publicConfig = require("../../util/publicConfig");
const { flags } = require("socket.io/lib/namespace");

// 列表 seal_recruit
// 查询：用get 其他：用post
// 通过手机号搜索查询
router.get("/info", function (req, response, next) {
  let phone = req.query.phone; //手机号
  var str = "";
  // 搜索框或者查询;
  if (phone) {
    str = str + " and s.phone = " + phone;
  }
  var sql = `SELECT
               s.id,
               s.userName,
               s.work,
               s.phone,
               s.city,
               s.company,
               s.social,
               s.good,
               s.remarks,
               s.Java,
               s.Vue,
               s.uniApp,
               s.product,
               s.project,
               s.test,
               s.ui,
               s.architect,
               s.style,
               s.mode,
               s.form,
               s.commitTime
            FROM seal_recruit s  where s.is_delete = 1`;
  if (str) sql = sql + str;
  db.query(sql, [], function (err, results, fields) {
    if (err) {
      apiUtil.responseResult(
        response,
        false,
        301,
        "系统异常，数据库执行失败：" + err,
        null
      );
    } else {
      var obj = {};
      obj.list = results;
      apiUtil.responseResult(response, true, 0, "", obj);
    }
  });
});

// 查询列表
router.get("/info/list", function (req, response, next) {
  let page = req.query.pageNum; //页码
  let pageSize = req.query.pageSize; //每页多少条
  // let phone = req.query.phone; //手机号
  pageSize = parseInt(pageSize);
  page = (parseInt(page) - 1) * pageSize;
  // var str = "";
  // // 搜索框或者查询;
  // if (phone) {
  //   str = str + " and s.phone = " + phone;
  // }
  db.query(
    `SELECT COUNT(s.id) total FROM seal_recruit s  where s.is_delete = 1`,
    [],
    function (err, results, fields) {
      let count = results;
      var sql = `SELECT
               s.id,
               s.userName,
               s.work,
               s.phone,
               s.city,
               s.company,
               s.social,
               s.good,
               s.remarks,
               s.Java,
               s.Vue,
               s.uniApp,
               s.product,
               s.project,
               s.test,
               s.ui,
               s.architect,
               s.style,
               s.mode,
               s.form,
               s.commitTime
            FROM seal_recruit s  where s.is_delete = 1 limit ${page},${pageSize} `;
      // if (str) sql = sql + str;
      if (err) {
        apiUtil.responseResult(response, false, 301, "系统异常：" + err, null);
      } else {
        db.query(sql, [], function (err, results, fields) {
          if (err) {
            apiUtil.responseResult(
              response,
              false,
              301,
              "系统异常，数据库执行失败：" + err,
              null
            );
          } else {
            var obj = {};
            obj.list = results;
            obj.count = count[0].total;
            obj.page = page + 1;
            obj.pageSize = pageSize;
            apiUtil.responseResult(response, true, 0, "", obj);
          }
        });
      }
    }
  );
});

// 添加信息;
router.post("/info/add", function (req, response, next) {
  var model = req.body;
  // 根据手机号查询用户是否已存在
  db.query(
    "select * from seal_recruit where phone = ?",
    [model.phone],
    function (err, results, fields) {
      if (results && results.length > 0) {
        apiUtil.responseResult(response, false, 301, "该用户已存在！", null);
      } else {
        var id = util.uuid();
        // 添加
        var sql = `INSERT INTO seal_recruit(   
          id,   
          userName,
          work,
          phone,
          city,
          company,
          social,
          good,
          remarks,
          Java,
          Vue,
          uniApp,
          product,
          project,
          test,
          ui,
          architect,
          style,
          mode,
          form,
          commitTime
        ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        var options = [
          util.uuid(),
          model.userName,
          model.work,
          model.phone,
          model.city,
          model.company,
          model.social,
          model.good,
          model.remarks,
          model.Java,
          model.Vue,
          model.uniApp,
          model.product,
          model.project,
          model.test,
          model.ui,
          model.architect,
          model.style,
          model.mode,
          model.form,
          model.commitTime,
        ];
        db.query(sql, options, function (err, results, fields) {
          if (err) {
            apiUtil.responseResult(
              response,
              false,
              301,
              "系统异常：" + err,
              null
            );
          } else {
            var obj = {};
            obj = results;
            obj.id = id;
            apiUtil.responseResult(response, true, 0, "", obj);
          }
        });
      }
    }
  );
});

// // 删除信息
router.post("/info/delete", function (req, response, next) {
  var id = req.body.id;
  if (!id) {
    apiUtil.responseResult(response, false, 300, "id不能为空", null);
    return;
  }
  // 先查询当前人是否存在
  db.query(
    "SELECT * FROM seal_recruit WHERE id = ? and is_delete = '1'",
    [id],
    function (err, results, fields) {
      if (results && results.length <= 0) {
        apiUtil.responseResult(response, false, 301, "该id不存在", null);
      } else {
        // 删除采用修改delete字段为0
        var sql = `UPDATE seal_recruit SET
      is_delete = '0'
      WHERE id = ? and is_delete = '1'`;
        db.query(sql, [id], function (err, results, fields) {
          if (err) {
            if (err.code == "ER_DUP_ENTRY") {
              apiUtil.responseResult(
                response,
                false,
                301,
                "该姓名已存在！",
                null
              );
            } else {
              apiUtil.responseResult(response, false, 301, "系统异常", null);
            }
          } else {
            var obj = {};
            obj = results;
            apiUtil.responseResult(response, true, 0, "", obj);
          }
        });
      }
    }
  );
});

module.exports = router;
