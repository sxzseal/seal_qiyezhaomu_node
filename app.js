var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
// 引用各业务模块的api接口服务
// 用户信息
var addInfo = require("./routes/user/index.js");
var getInfo = require("./routes/user/index.js");
var deleteInfo = require("./routes/user/index.js");
var userInfo = require("./routes/user/index.js");

// 引入log4j,并且加载配置文件
var log4js = require("log4js");
log4js.configure("./config/log4j.json");

var app = express();
var cors = require("cors");
app.use(cors());
//设置跨域访问
app.all("*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By", " 3.2.1");
  res.header("Content-Type", "application/json;charset=utf-8");
  // next();
  console.log("app.all ****** :", req.headers);
  // if (!req.headers.authorization){
  //     next('权限不足，请先登录...')
  // }else{
  //     next()
  // }

  // res.header("Access-Control-Allow-Origin", "*");
  // res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With,token, sign');
  // res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  // res.header("X-Powered-By", ' 3.2.1')
  // res.header("Content-Type", "application/json;charset=utf-8");
  if (req.method == "OPTIONS") {
    // res.send(200); /让options请求快速返回/
    res.sendStatus(200);
  } else {
    console.log("req.url::", req.url);
    // 需要校验token
    // if (req.url.indexOf("/api") == -1 && req.url.indexOf("/upload") == -1) {
    //   let token = req.headers.authorization;
    //   console.log(
    //     "token 000:",
    //     token,
    //     req.url.indexOf("/api/login") == -1,
    //     req.url.indexOf("/upload") == -1
    //   );

    //   if (token) {
    //     try {
    //       let secret = publicConfig.jwtTokenSecret;
    //       console.log("secret:", secret);

    //       var decoded = jwt.decode(token, secret);
    //       console.log("decoded:", decoded);
    //       console.log("Date.now():", Date.now());
    //       if (decoded.exp <= Date.now() / 1000) {
    //         // java服务器记录值为秒
    //         res.sendStatus(401);
    //         return;
    //       }
    //     } catch (err) {
    //       console.log("err:", err);

    //       res.sendStatus(401);
    //       return;
    //     }
    //   } else {
    //     res.sendStatus(401);
    //     return;
    //   }
    // }
    next();
  }
});
// app.use(retoken)

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(log4js.connectLogger(log4js.getLogger("http"), { level: "trace" }));

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, "./public")));

// 把引用的接口服务注册到/api目录下
app.use("/api", addInfo);
app.use("/api", getInfo);
app.use("/api", deleteInfo);
app.use("/api", userInfo);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
