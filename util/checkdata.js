exports.checkRole = checkRole;
exports.checkFileType = checkFileType;
exports.checkGender = checkGender;
exports.checkHttp = checkHttp;

/**
 * 验证角色
 * 1：教师
 * 2：学生
 */
function checkRole(role) {
    if (role == 1 || role == 2 || role == 3) {
        return true;
    }
    return false;
}

/**
 *1：文本
 2：图片
 3：视频
 4：音频
 */
function checkFileType(type) {
    if (type == 1 || type == 2 || type == 3 || type == 4) {
        return true;
    }
    return false;
}

/**
 *1：男
 2：女
 */
function checkGender(type) {
    if (type == 1 || type == 2) {
        return true;
    }
    return false;
}

/**
 * 判断url前缀是不是http或https
 * @param url
 * @returns {boolean}
 */
function checkHttp(url) {
    if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0) {
        return true;
    }
    return false;
}