/*
	引用外部服务配置路径
*/
exports.Environment = {
  AlgorithmicServices: 'http://118.190.210.61:5000', // 算法服务地址
  // HeartApiServices: 'http://localhost:5090', // 本地开发api服务地址
  HeartApiServices: 'http://47.105.192.255:5090', // 线上api服务地址
};
