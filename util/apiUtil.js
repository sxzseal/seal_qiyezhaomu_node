exports.responseResult = responseResult;
var logger = require('log4js').getLogger("response");

function responseResult(res, isSuccess, errorCode, errorMessage, jsonObject) {
    // {code: ,errorCode: 0,errorMessage: “”,data:{}}
    var result = new Object();
    var code = 200;//成功
    var msg = "ok";
    if (!isSuccess) {
        code = errorCode;
        msg = errorMessage;
    }

    result.code = code;
    result.success = msg;
    result.result = jsonObject;
    res.writeHead(200, { 'Content-Type': 'application/json;charset=utf-8'});//设置response编码为utf-8
    var msg = JSON.stringify(result);
    logger.info(msg);
    // console.log("返回结果："+JSON.stringify(result));
    res.end(msg);
}
