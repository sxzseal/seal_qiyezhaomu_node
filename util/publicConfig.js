var publicConfig = {
    debug: true, // debug 为 true 时，用于本地调试
    jwtTokenSecret: 'cOrP@QiZcjxPEnQy',// jwt密钥
    defaultVersion: 'v1_1' , // 默认算法最新版本
    videoCutRatio: '1016x708', // 视频切图的比例大小
};


module.exports = publicConfig;