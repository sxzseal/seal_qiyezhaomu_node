var mysql = require("mysql");
var async = require("async");
var publicConfig = require("./publicConfig");

// 本地开发库
var pool = mysql.createPool({
  host: "rm-2ze8ix960t1xinyr7.mysql.rds.aliyuncs.com",
  user: "dbuser_zhaomu",
  password: "qBmVvq2TncyN9iCc",
  database: "db_zhaomu",
  port: 3306,
});

function query(sql, options, callback) {
  if (publicConfig.debug) {
    console.log("sql:" + sql + " === " + options);
  }
  pool.getConnection(function (err, conn) {
    if (err) {
      callback(err, null, null);
    } else {
      conn.query(sql, options, function (err, results, fields) {
        //释放连接
        conn.release();
        //事件驱动回调
        callback(err, results, fields);
      });
    }
  });
}

function getNewSqlParamEntity(sql, params, callback) {
  if (callback) {
    return callback(null, {
      sql: sql,
      params: params,
    });
  }
  return {
    sql: sql,
    params: params,
  };
}

function execTrans(sqlparamsEntities, callback) {
  pool.getConnection(function (err, connection) {
    if (err) {
      return callback(err, null);
    }
    connection.beginTransaction(function (err) {
      if (err) {
        return callback(err, null);
      }
      console.log(
        "开始执行transaction，共执行" + sqlparamsEntities.length + "条数据"
      );
      var funcAry = [];
      sqlparamsEntities.forEach(function (sql_param) {
        var temp = function (cb) {
          var sql = sql_param.sql;
          var param = sql_param.params;
          connection.query(sql, param, function (tErr, rows, fields) {
            if (tErr) {
              connection.rollback(function () {
                console.log("事务失败，" + sql_param + "，ERROR：" + tErr);
                throw tErr;
              });
            } else {
              return cb(null, "ok");
            }
          });
        };
        funcAry.push(temp);
      });

      async.series(funcAry, function (err, result) {
        console.log("transaction error: " + err);
        if (err) {
          connection.rollback(function (err) {
            console.log("transaction error: " + err);
            connection.release();
            return callback(err, null);
          });
        } else {
          connection.commit(function (err, info) {
            console.log("transaction info: " + JSON.stringify(info));
            if (err) {
              console.log("执行事务失败，" + err);
              connection.rollback(function (err) {
                console.log("transaction error: " + err);
                connection.release();
                return callback(err, null);
              });
            } else {
              connection.release();
              return callback(null, info);
            }
          });
        }
      });
    });
  });
}

exports.query = query;
exports.getNewSqlParamEntity = getNewSqlParamEntity;
exports.execTrans = execTrans;
